import CalendarIcon from "~/assets/img/calendar.svg?inline";

export default {
  props: ["to", "type", "content", "icon"],
  components: {
    CalendarIcon,
  },
};
