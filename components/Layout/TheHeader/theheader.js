import ChevronUp from "~/assets/img/chevron-up.svg?inline";

export default {
  components: { ChevronUp },

  props: ['logged'],

  data() {
    return {
      menuOpened: false,
    };
  },

  computed: {
    async isLogged() {
      return this.$admin.auth.currentUser;
    }
  },

  mounted() {
    this.handleScroll()
    this.handleSubMenu()
  },

  watch: {
    $route() {
      this.menuOpened = false
    }
  },

  methods: {
    backToTop() {
      window.scrollTo(0, 0);
    },

    toggle() {
      this.menuOpened = !this.menuOpened;
    },

    handleScroll() {
      const header = this.$refs.header;
      const subheader = this.$refs.subheader;
      document.addEventListener("scroll", () => {
        const scroll = window.pageYOffset;
        if (scroll > 200) {
          header.classList.add("reveal");
          subheader.classList.add("reveal");
        } else {
          header.classList.remove("reveal");
          subheader.classList.remove("reveal");
        }
      });
    },

    handleSubMenu() {
      const mainmenu = this.$refs.mainMenu
      const menuItems = mainmenu.querySelectorAll('li')
      menuItems.forEach((item) => {
        const subMenu = item.querySelector('ul')
        if(subMenu) {
          item.addEventListener('mouseenter', () => {
            subMenu.classList.add('display')
          })

          item.addEventListener("mouseleave", () => {
            subMenu.classList.remove("display");
          });
        }
      })
    },

    logout() {
      this.$admin.signOut();
      this.$router.push('/login')
    }
  },
};
