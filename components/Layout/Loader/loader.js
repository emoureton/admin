export default {
  props: ["color"],
  mounted() {
    this.$refs.loader.style.border = "8px solid " + this.color;
    this.$refs.loader.style.borderColor =
      this.color + " transparent transparent transparent";
  },
};
