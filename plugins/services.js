import { AdminApi } from "../service/admin";
import { AtelierBoussoleApi } from "../service/api";

export default ({ app }, inject) => {
  const api = new AtelierBoussoleApi();
  inject("atelierboussole", api);

  const admin = new AdminApi();
  inject("admin", admin);
};
