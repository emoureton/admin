import axios from 'axios'

export class AtelierBoussoleApi {
  constructor() {}

  // ORDERS
  createOrder = ({ payload }) => {
    try {
      const req = axios.post("/api/order/create", payload);
      const data = req.then((r) => r.data);
      return data;
    } catch (e) {
      return e;
    }
  };

  getOrder = ({ orderID }) => {
    try {
      const req = axios.get("/api/order/" + orderID);
      const data = req.then((r) => r.data);
      return data;
    } catch (e) {
      return e;
    }
  };

  getOrdersByCode = ({ code }) => {
    try {
      const req = axios.get("/api/order/by/" + code);
      const data = req.then((r) => r.data);
      return data;
    } catch (e) {
      return e;
    }
  };

  editOrder = ({ orderID, payload }) => {
    try {
      const req = axios.post("/api/order/set/" + orderID, payload);
      const data = req.then((r) => r.data);
      return data;
    } catch (e) {
      return e;
    }
  };

  // CODES
  createCode = ({ payload }) => {
    try {
      const req = axios.post("/api/code/create", payload);
      const data = req.then((r) => r.data);
      return data;
    } catch (e) {
      return e;
    }
  };

  getCode = ({ code }) => {
    try {
      const req = axios.get("/api/code/" + code);
      const data = req.then((r) => r.data);
      return data;
    } catch (e) {
      return e;
    }
  };

  listCodes = () => {
    try {
      const req = axios.get("/api/code/list");
      const data = req.then((r) => r.data);
      return data;
    } catch (e) {
      return e;
    }
  };

  editCode = ({ codeID, payload }) => {
    try {
      const req = axios.post("/api/code/set/" + codeID, payload);
      const data = req.then((r) => r.data);
      return data;
    } catch (e) {
      return e;
    }
  };

  deleteCode = ({ code }) => {
    try {
      const req = axios.post("/api/code/delete/" + code);
      const data = req.then((r) => r.data);
      return data;
    } catch (e) {
      return e;
    }
  };
}
