import { initializeApp } from "firebase/app"
import {
  getAuth,
  signInWithEmailAndPassword,
  sendPasswordResetEmail,
  signOut,
  setPersistence,
  browserLocalPersistence,
} from "firebase/auth";

import { config } from "../utils/firebase";

export class AdminApi {
  constructor() {
    initializeApp(config);
    this.auth = getAuth();
  }

  async getAuthStatus() {
    return getAuth()
  }

  login = async ({ email, password }) => {
    try {
      return await setPersistence(this.auth, browserLocalPersistence)
        .then(() => {
          return signInWithEmailAndPassword(this.auth, email, password);
        })
        .catch((error) => {
          return error
        });
    } catch (e) {
      return new Error(e)
    }
  }

  async resetPassword(email) {
    return sendPasswordResetEmail(this.auth, email)
      .then(() => {
        return 'Mail de réinitialisation de mot de passe envoyé'
      })
      .catch((e) => {
        return "Compte inexistant";
      });
  }

  signOut() {
    signOut(this.auth).then(() => {
      console.log('déconnecté')
      console.log(this.auth)
    }).catch((err) => {
      console.log(err)
    })
  }
}
