export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "L'atelier & La boussole",
    htmlAttrs: {
      lang: "fr",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
      { name: "robots", content: "noindex" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    {
      src: "~/assets/styles/global.scss",
      lang: "scss",
    },
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [{ src: "~/plugins/services.js" }],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: [{ path: "~/components", extensions: ["vue"] }],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    "@nuxt/image",
    [
      "@nuxtjs/google-fonts",
      {
        families: {
          Poppins: {
            wght: [300, 400, 600, 700],
          },
        },
        display: "swap",
        prefetch: true,
        preconnect: true,
        preload: true,
        download: true,
        base64: false,
        stylePath: "styles/_fonts.scss",
      },
    ],
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    "@nuxtjs/axios",
    "nuxt-sm",
    "@nuxtjs/style-resources",
    "@nuxtjs/svg",
    "@nuxt/image",
    ["cookie-universal-nuxt", { alias: "cookies" }],
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: "/",
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  styleResources: {
    scss: [
      "~/assets/styles/_breakpoints.scss",
      "~/assets/styles/_animations.scss",
      "~/assets/styles/_variables.scss",
    ],
  },

  env: {
    FIRESTORE_PROJECT_ID: process.env.FIRESTORE_PROJECT_ID,
    APP_URL: process.env.APP_URL,
    FRONT_URL: process.env.FRONT_URL,
    SERVICE_ACCOUNT_MAIL: process.env.SERVICE_ACCOUNT_MAIL,
    SERVICE_ACCOUNT_KEY: process.env.SERVICE_ACCOUNT_KEY,
    ADMIN_EMAILS: process.env.ADMIN_EMAILS,
  },

  serverMiddleware: [{ path: "/api", handler: "~/api" }],

  image: {
    provider: "static",
  },
};
