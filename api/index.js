const express = require("express");
const app = express();
const bodyParser = require("body-parser");

app.use(bodyParser.json());

const codes = require("./codes.js");
app.use(codes);

if (require.main === module) {
  const port = 3001;
  app.listen(port, () => {
    console.log(`API server listening on port ${port}`);
  });
}

module.exports = app;
