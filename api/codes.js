const express = require('express')
const router = express.Router()

import { db, fs } from './utils/db'
 
router.get("/order/by/:id", async (req, res) => {
  let result = [];
  const order = await db.collection("orders").where('code.id', '==', req.params.id).get();
  order.forEach((el) => {
    const item = [];
    item.push(el.id);
    item.push(el.data());
    result.push(item);
  });
  res.json(result);
});

router.get('/order/:id', async (req, res) => {
  const order = await db.collection('orders').doc(req.params.id).get();
  if (!order.exists) {
    res.json('No document');
   } else {
    res.json(order.data());
   }
})

router.post('/order/set/:id', async (req, res) => {
  const order = await db.collection('orders').doc(req.params.id).update(req.body);
  res.json('Updated')
})

router.post('/code/create', async (req, res) => {
  const code = await db.collection("codes").add(req.body);
  res.json(code.id);
})

router.post("/code/set/:id", async (req, res) => {
  const code = await db
    .collection("codes")
    .doc(req.params.id)
    .update(req.body);
  res.json("Updated");
});

router.get("/code/list", async (req, res) => {
  let result = [];
  const codes = await db.collection("codes").get();
  codes.forEach((doc) => {
    const item = []
    item.push(doc.id);
    item.push(doc.data());
    result.push(item)
  });
  res.json(result);
});

router.get("/code/:id", async (req, res) => {
  const code = await db.collection("codes").doc(req.params.id).get();
  if (!code.exists) {
    res.json("No document");
  } else {
    res.json(code.data());
  }
});

router.post("/code/delete/:id", async (req, res) => {
  const code = await db.collection("codes").doc(req.params.id).delete();
  res.json("Deleted");
});

module.exports = router