const fs = require("firebase-admin");

if (fs.apps.length === 0) {
  fs.initializeApp({
    credential: fs.credential.cert({
      private_key:
        process.env.SERVICE_ACCOUNT_KEY[0] === "-"
          ? process.env.SERVICE_ACCOUNT_KEY
          : JSON.parse(process.env.SERVICE_ACCOUNT_KEY),
      client_email: process.env.SERVICE_ACCOUNT_MAIL,
      projectId: process.env.FIRESTORE_PROJECT_ID,
    }),
    projectId: process.env.FIRESTORE_PROJECT_ID,
  });
}

const db = fs.firestore();

export {
  fs,
  db
};
