import MyComponent from '../../../../slices/TextElement';
import SliceZone from 'vue-slicezone'

export default {
  title: 'slices/TextElement'
}


export const _Default = () => ({
  components: {
    MyComponent,
    SliceZone
  },
  methods: {
    resolve() {
      return MyComponent
    }
  },
  data() {
    return {
      mock: {"variation":"default","name":"Default","slice_type":"text_element","items":[],"primary":{"description":[{"type":"paragraph","text":"Sunt mollit est esse nisi ad ex ex ex exercitation dolor exercitation elit mollit. Velit aliqua minim ipsum minim id dolore ut fugiat sit sit magna.","spans":[]}]},"id":"_Default"}
    }
  },
  template: '<SliceZone :slices="[mock]" :resolver="resolve" />'
})
_Default.storyName = 'Default'
